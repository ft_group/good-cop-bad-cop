from treestory import *

story = TreeStory(7)

#Layer 0
story.addToLayer(StoryNode("Hello Driver", {
    "A": { 'text': "Good Cop", 'connection': [1,0] },
    "B": { 'text': "Bad Cop", 'connection': [1,1] }
}), 0)

#Layer 1
story.addToLayer(StoryNode("Hello Driver", {
    "A": { 'text': "Good Cop", 'connection': [2,0] },
    "B": { 'text': "Bad Cop", 'connection': [2,1] }
}), 1)
story.addToLayer(StoryNode("Hello Driver", {
    "A": { 'text': "Good Cop", 'connection': [2,2] },
    "B": { 'text': "Bad Cop", 'connection': [2,3] }
}), 1)

#Layer 2
story.addToLayer(StoryNode("Hello Driver", {
    "A": { 'text': "Good Cop", 'connection': [3,0] },
    "B": { 'text': "Bad Cop", 'connection': [3,1] }
}), 2)
story.addToLayer(StoryNode("Hello Driver", {
    "A": { 'text': "Good Cop", 'connection': [3,2] },
    "B": { 'text': "Bad Cop", 'connection': [3,3] }
}), 2)
story.addToLayer(StoryNode("Hello Driver", {
    "A": { 'text': "Good Cop", 'connection': [3,4] },
    "B": { 'text': "Bad Cop", 'connection': [3,5] }
}), 2)
story.addToLayer(StoryNode("Hello Driver", {
    "A": { 'text': "Good Cop", 'connection': [3,6] },
    "B": { 'text': "Bad Cop", 'connection': [3,7] }
}), 2)

#Layer 3
story.addToLayer(StoryNode("Hello Driver", {
    "A": { 'text': "Good Cop", 'connection': [4,0] },
    "B": { 'text': "Bad Cop", 'connection': [4,1] }
}), 3)
story.addToLayer(StoryNode("Hello Driver", {
    "A": { 'text': "Good Cop", 'connection': [4,2] },
    "B": { 'text': "Bad Cop", 'connection': [4,3] }
}), 3)
story.addToLayer(StoryNode("Hello Driver", {
    "A": { 'text': "Good Cop", 'connection': [4,4] },
    "B": { 'text': "Bad Cop", 'connection': [4,5] }
}), 3)
story.addToLayer(StoryNode("Hello Driver", {
    "A": { 'text': "Good Cop", 'connection': [4,6] },
    "B": { 'text': "Bad Cop", 'connection': [4,7] }
}), 3)
story.addToLayer(StoryNode("Hello Driver", {
    "A": { 'text': "Good Cop", 'connection': [4,8] },
    "B": { 'text': "Bad Cop", 'connection': [4,9] }
}), 3)
story.addToLayer(StoryNode("Hello Driver", {
    "A": { 'text': "Good Cop", 'connection': [4,10] },
    "B": { 'text': "Bad Cop", 'connection': [4,11] }
}), 3)
story.addToLayer(StoryNode("Hello Driver", {
    "A": { 'text': "Good Cop", 'connection': [4,12] },
    "B": { 'text': "Bad Cop", 'connection': [4,13] }
}), 3)
story.addToLayer(StoryNode("Hello Driver", {
    "A": { 'text': "Good Cop", 'connection': [4,14] },
    "B": { 'text': "Bad Cop", 'connection': [4,15] }
}), 3)

#Layer 4
story.addToLayer(StoryNode("Hello Driver", {
    "A": { 'text': "Good Cop", 'connection': [5,0] },
    "B": { 'text': "Bad Cop", 'connection': [5,1] }
}), 4)
story.addToLayer(StoryNode("Hello Driver", {
    "A": { 'text': "Good Cop", 'connection': [5,2] },
    "B": { 'text': "Bad Cop", 'connection': [5,3] }
}), 4)
story.addToLayer(StoryNode("Hello Driver", {
    "A": { 'text': "Good Cop", 'connection': [5,4] },
    "B": { 'text': "Bad Cop", 'connection': [5,5] }
}), 4)
story.addToLayer(StoryNode("Hello Driver", {
    "A": { 'text': "Good Cop", 'connection': [5,6] },
    "B": { 'text': "Bad Cop", 'connection': [5,7] }
}), 4)
story.addToLayer(StoryNode("Hello Driver", {
    "A": { 'text': "Good Cop", 'connection': [5,8] },
    "B": { 'text': "Bad Cop", 'connection': [5,9] }
}), 4)
story.addToLayer(StoryNode("Hello Driver", {
    "A": { 'text': "Good Cop", 'connection': [5,10] },
    "B": { 'text': "Bad Cop", 'connection': [5,11] }
}), 4)
story.addToLayer(StoryNode("Hello Driver", {
    "A": { 'text': "Good Cop", 'connection': [5,12] },
    "B": { 'text': "Bad Cop", 'connection': [5,13] }
}), 4)
story.addToLayer(StoryNode("Hello Driver", {
    "A": { 'text': "Good Cop", 'connection': [5,14] },
    "B": { 'text': "Bad Cop", 'connection': [5,15] }
}), 4)
story.addToLayer(StoryNode("Hello Driver", {
    "A": { 'text': "Good Cop", 'connection': [5,16] },
    "B": { 'text': "Bad Cop", 'connection': [5,17] }
}), 4)
story.addToLayer(StoryNode("Hello Driver", {
    "A": { 'text': "Good Cop", 'connection': [5,18] },
    "B": { 'text': "Bad Cop", 'connection': [5,19] }
}), 4)
story.addToLayer(StoryNode("Hello Driver", {
    "A": { 'text': "Good Cop", 'connection': [5,20] },
    "B": { 'text': "Bad Cop", 'connection': [5,21] }
}), 4)
story.addToLayer(StoryNode("Hello Driver", {
    "A": { 'text': "Good Cop", 'connection': [5,22] },
    "B": { 'text': "Bad Cop", 'connection': [5,23] }
}), 4)
story.addToLayer(StoryNode("Hello Driver", {
    "A": { 'text': "Good Cop", 'connection': [5,24] },
    "B": { 'text': "Bad Cop", 'connection': [5,25] }
}), 4)
story.addToLayer(StoryNode("Hello Driver", {
    "A": { 'text': "Good Cop", 'connection': [5,26] },
    "B": { 'text': "Bad Cop", 'connection': [5,27] }
}), 4)
story.addToLayer(StoryNode("Hello Driver", {
    "A": { 'text': "Good Cop", 'connection': [5,28] },
    "B": { 'text': "Bad Cop", 'connection': [5,29] }
}), 4)
story.addToLayer(StoryNode("Hello Driver", {
    "A": { 'text': "Good Cop", 'connection': [5,30] },
    "B": { 'text': "Bad Cop", 'connection': [5,31] }
}), 4)

#Layer 5
story.addToLayer(StoryNode("Hello Driver", {
    "A": { 'text': "Good Cop", 'connection': [6,0] },
    "B": { 'text': "Bad Cop", 'connection': [6,1] }
}), 5)
story.addToLayer(StoryNode("Hello Driver", {
    "A": { 'text': "Good Cop", 'connection': [6,2] },
    "B": { 'text': "Bad Cop", 'connection': [6,3] }
}), 5)
story.addToLayer(StoryNode("Hello Driver", {
    "A": { 'text': "Good Cop", 'connection': [6,4] },
    "B": { 'text': "Bad Cop", 'connection': [6,5] }
}), 5)
story.addToLayer(StoryNode("Hello Driver", {
    "A": { 'text': "Good Cop", 'connection': [6,6] },
    "B": { 'text': "Bad Cop", 'connection': [6,7] }
}), 5)
story.addToLayer(StoryNode("Hello Driver", {
    "A": { 'text': "Good Cop", 'connection': [6,8] },
    "B": { 'text': "Bad Cop", 'connection': [6,9] }
}), 5)
story.addToLayer(StoryNode("Hello Driver", {
    "A": { 'text': "Good Cop", 'connection': [6,10] },
    "B": { 'text': "Bad Cop", 'connection': [6,11] }
}), 5)
story.addToLayer(StoryNode("Hello Driver", {
    "A": { 'text': "Good Cop", 'connection': [6,12] },
    "B": { 'text': "Bad Cop", 'connection': [6,13] }
}), 5)
story.addToLayer(StoryNode("Hello Driver", {
    "A": { 'text': "Good Cop", 'connection': [6,14] },
    "B": { 'text': "Bad Cop", 'connection': [6,15] }
}), 5)
story.addToLayer(StoryNode("Hello Driver", {
    "A": { 'text': "Good Cop", 'connection': [6,16] },
    "B": { 'text': "Bad Cop", 'connection': [6,17] }
}), 5)
story.addToLayer(StoryNode("Hello Driver", {
    "A": { 'text': "Good Cop", 'connection': [6,18] },
    "B": { 'text': "Bad Cop", 'connection': [6,19] }
}), 5)
story.addToLayer(StoryNode("Hello Driver", {
    "A": { 'text': "Good Cop", 'connection': [6,20] },
    "B": { 'text': "Bad Cop", 'connection': [6,21] }
}), 5)
story.addToLayer(StoryNode("Hello Driver", {
    "A": { 'text': "Good Cop", 'connection': [6,22] },
    "B": { 'text': "Bad Cop", 'connection': [6,23] }
}), 5)
story.addToLayer(StoryNode("Hello Driver", {
    "A": { 'text': "Good Cop", 'connection': [6,24] },
    "B": { 'text': "Bad Cop", 'connection': [6,25] }
}), 5)
story.addToLayer(StoryNode("Hello Driver", {
    "A": { 'text': "Good Cop", 'connection': [6,26] },
    "B": { 'text': "Bad Cop", 'connection': [6,27] }
}), 5)
story.addToLayer(StoryNode("Hello Driver", {
    "A": { 'text': "Good Cop", 'connection': [6,28] },
    "B": { 'text': "Bad Cop", 'connection': [6,29] }
}), 5)
story.addToLayer(StoryNode("Hello Driver", {
    "A": { 'text': "Good Cop", 'connection': [6,30] },
    "B": { 'text': "Bad Cop", 'connection': [6,31] }
}), 5)
story.addToLayer(StoryNode("Hello Driver", {
    "A": { 'text': "Good Cop", 'connection': [6,32] },
    "B": { 'text': "Bad Cop", 'connection': [6,33] }
}), 5)
story.addToLayer(StoryNode("Hello Driver", {
    "A": { 'text': "Good Cop", 'connection': [6,34] },
    "B": { 'text': "Bad Cop", 'connection': [6,35] }
}), 5)
story.addToLayer(StoryNode("Hello Driver", {
    "A": { 'text': "Good Cop", 'connection': [6,36] },
    "B": { 'text': "Bad Cop", 'connection': [6,37] }
}), 5)
story.addToLayer(StoryNode("Hello Driver", {
    "A": { 'text': "Good Cop", 'connection': [6,38] },
    "B": { 'text': "Bad Cop", 'connection': [6,39] }
}), 5)
story.addToLayer(StoryNode("Hello Driver", {
    "A": { 'text': "Good Cop", 'connection': [6,40] },
    "B": { 'text': "Bad Cop", 'connection': [6,41] }
}), 5)
story.addToLayer(StoryNode("Hello Driver", {
    "A": { 'text': "Good Cop", 'connection': [6,42] },
    "B": { 'text': "Bad Cop", 'connection': [6,43] }
}), 5)
story.addToLayer(StoryNode("Hello Driver", {
    "A": { 'text': "Good Cop", 'connection': [6,44] },
    "B": { 'text': "Bad Cop", 'connection': [6,45] }
}), 5)
story.addToLayer(StoryNode("Hello Driver", {
    "A": { 'text': "Good Cop", 'connection': [6,46] },
    "B": { 'text': "Bad Cop", 'connection': [6,47] }
}), 5)
story.addToLayer(StoryNode("Hello Driver", {
    "A": { 'text': "Good Cop", 'connection': [6,48] },
    "B": { 'text': "Bad Cop", 'connection': [6,49] }
}), 5)
story.addToLayer(StoryNode("Hello Driver", {
    "A": { 'text': "Good Cop", 'connection': [6,50] },
    "B": { 'text': "Bad Cop", 'connection': [6,51] }
}), 5)
story.addToLayer(StoryNode("Hello Driver", {
    "A": { 'text': "Good Cop", 'connection': [6,52] },
    "B": { 'text': "Bad Cop", 'connection': [6,53] }
}), 5)
story.addToLayer(StoryNode("Hello Driver", {
    "A": { 'text': "Good Cop", 'connection': [6,54] },
    "B": { 'text': "Bad Cop", 'connection': [6,55] }
}), 5)
story.addToLayer(StoryNode("Hello Driver", {
    "A": { 'text': "Good Cop", 'connection': [6,56] },
    "B": { 'text': "Bad Cop", 'connection': [6,57] }
}), 5)
story.addToLayer(StoryNode("Hello Driver", {
    "A": { 'text': "Good Cop", 'connection': [6,58] },
    "B": { 'text': "Bad Cop", 'connection': [6,59] }
}), 5)
story.addToLayer(StoryNode("Hello Driver", {
    "A": { 'text': "Good Cop", 'connection': [6,60] },
    "B": { 'text': "Bad Cop", 'connection': [6,61] }
}), 5)
story.addToLayer(StoryNode("Hello Driver", {
    "A": { 'text': "Good Cop", 'connection': [6,62] },
    "B": { 'text': "Bad Cop", 'connection': [6,63] }
}), 5)

#Layer 6
story.addToLayer(StoryNode("Hello Driver", {
    "q": { 'text': "start over", 'connection': [0,0] },
}), 6)
story.addToLayer(StoryNode("Hello Driver", {
    "q": { 'text': "start over", 'connection': [0,0] },
}), 6)
story.addToLayer(StoryNode("Hello Driver", {
    "q": { 'text': "start over", 'connection': [0,0] },
}), 6)
story.addToLayer(StoryNode("Hello Driver", {
    "q": { 'text': "start over", 'connection': [0,0] },
}), 6)
story.addToLayer(StoryNode("Hello Driver", {
    "q": { 'text': "start over", 'connection': [0,0] },
}), 6)
story.addToLayer(StoryNode("Hello Driver", {
    "q": { 'text': "start over", 'connection': [0,0] },
}), 6)
story.addToLayer(StoryNode("Hello Driver", {
    "q": { 'text': "start over", 'connection': [0,0] },
}), 6)
story.addToLayer(StoryNode("Hello Driver", {
    "q": { 'text': "start over", 'connection': [0,0] },
}), 6)
story.addToLayer(StoryNode("Hello Driver", {
    "q": { 'text': "start over", 'connection': [0,0] },
}), 6)
story.addToLayer(StoryNode("Hello Driver", {
    "q": { 'text': "start over", 'connection': [0,0] },
}), 6)
story.addToLayer(StoryNode("Hello Driver", {
    "q": { 'text': "start over", 'connection': [0,0] },
}), 6)
story.addToLayer(StoryNode("Hello Driver", {
    "q": { 'text': "start over", 'connection': [0,0] },
}), 6)
story.addToLayer(StoryNode("Hello Driver", {
    "q": { 'text': "start over", 'connection': [0,0] },
}), 6)
story.addToLayer(StoryNode("Hello Driver", {
    "q": { 'text': "start over", 'connection': [0,0] },
}), 6)
story.addToLayer(StoryNode("Hello Driver", {
    "q": { 'text': "start over", 'connection': [0,0] },
}), 6)
story.addToLayer(StoryNode("Hello Driver", {
    "q": { 'text': "start over", 'connection': [0,0] },
}), 6)
story.addToLayer(StoryNode("Hello Driver", {
    "q": { 'text': "start over", 'connection': [0,0] },
}), 6)
story.addToLayer(StoryNode("Hello Driver", {
    "q": { 'text': "start over", 'connection': [0,0] },
}), 6)
story.addToLayer(StoryNode("Hello Driver", {
    "q": { 'text': "start over", 'connection': [0,0] },
}), 6)
story.addToLayer(StoryNode("Hello Driver", {
    "q": { 'text': "start over", 'connection': [0,0] },
}), 6)
story.addToLayer(StoryNode("Hello Driver", {
    "q": { 'text': "start over", 'connection': [0,0] },
}), 6)
story.addToLayer(StoryNode("Hello Driver", {
    "q": { 'text': "start over", 'connection': [0,0] },
}), 6)
story.addToLayer(StoryNode("Hello Driver", {
    "q": { 'text': "start over", 'connection': [0,0] },
}), 6)
story.addToLayer(StoryNode("Hello Driver", {
    "q": { 'text': "start over", 'connection': [0,0] },
}), 6)
story.addToLayer(StoryNode("Hello Driver", {
    "q": { 'text': "start over", 'connection': [0,0] },
}), 6)
story.addToLayer(StoryNode("Hello Driver", {
    "q": { 'text': "start over", 'connection': [0,0] },
}), 6)
story.addToLayer(StoryNode("Hello Driver", {
    "q": { 'text': "start over", 'connection': [0,0] },
}), 6)
story.addToLayer(StoryNode("Hello Driver", {
    "q": { 'text': "start over", 'connection': [0,0] },
}), 6)
story.addToLayer(StoryNode("Hello Driver", {
    "q": { 'text': "start over", 'connection': [0,0] },
}), 6)
story.addToLayer(StoryNode("Hello Driver", {
    "q": { 'text': "start over", 'connection': [0,0] },
}), 6)
story.addToLayer(StoryNode("Hello Driver", {
    "q": { 'text': "start over", 'connection': [0,0] },
}), 6)
story.addToLayer(StoryNode("Hello Driver", {
    "q": { 'text': "start over", 'connection': [0,0] },
}), 6)
story.addToLayer(StoryNode("Hello Driver", {
    "q": { 'text': "start over", 'connection': [0,0] },
}), 6)
story.addToLayer(StoryNode("Hello Driver", {
    "q": { 'text': "start over", 'connection': [0,0] },
}), 6)
story.addToLayer(StoryNode("Hello Driver", {
    "q": { 'text': "start over", 'connection': [0,0] },
}), 6)
story.addToLayer(StoryNode("Hello Driver", {
    "q": { 'text': "start over", 'connection': [0,0] },
}), 6)
story.addToLayer(StoryNode("Hello Driver", {
    "q": { 'text': "start over", 'connection': [0,0] },
}), 6)
story.addToLayer(StoryNode("Hello Driver", {
    "q": { 'text': "start over", 'connection': [0,0] },
}), 6)
story.addToLayer(StoryNode("Hello Driver", {
    "q": { 'text': "start over", 'connection': [0,0] },
}), 6)
story.addToLayer(StoryNode("Hello Driver", {
    "q": { 'text': "start over", 'connection': [0,0] },
}), 6)
story.addToLayer(StoryNode("Hello Driver", {
    "q": { 'text': "start over", 'connection': [0,0] },
}), 6)
story.addToLayer(StoryNode("Hello Driver", {
    "q": { 'text': "start over", 'connection': [0,0] },
}), 6)
story.addToLayer(StoryNode("Hello Driver", {
    "q": { 'text': "start over", 'connection': [0,0] },
}), 6)
story.addToLayer(StoryNode("Hello Driver", {
    "q": { 'text': "start over", 'connection': [0,0] },
}), 6)
story.addToLayer(StoryNode("Hello Driver", {
    "q": { 'text': "start over", 'connection': [0,0] },
}), 6)
story.addToLayer(StoryNode("Hello Driver", {
    "q": { 'text': "start over", 'connection': [0,0] },
}), 6)
story.addToLayer(StoryNode("Hello Driver", {
    "q": { 'text': "start over", 'connection': [0,0] },
}), 6)
story.addToLayer(StoryNode("Hello Driver", {
    "q": { 'text': "start over", 'connection': [0,0] },
}), 6)
story.addToLayer(StoryNode("Hello Driver", {
    "q": { 'text': "start over", 'connection': [0,0] },
}), 6)
story.addToLayer(StoryNode("Hello Driver", {
    "q": { 'text': "start over", 'connection': [0,0] },
}), 6)
story.addToLayer(StoryNode("Hello Driver", {
    "q": { 'text': "start over", 'connection': [0,0] },
}), 6)
story.addToLayer(StoryNode("Hello Driver", {
    "q": { 'text': "start over", 'connection': [0,0] },
}), 6)
story.addToLayer(StoryNode("Hello Driver", {
    "q": { 'text': "start over", 'connection': [0,0] },
}), 6)
story.addToLayer(StoryNode("Hello Driver", {
    "q": { 'text': "start over", 'connection': [0,0] },
}), 6)
story.addToLayer(StoryNode("Hello Driver", {
    "q": { 'text': "start over", 'connection': [0,0] },
}), 6)
story.addToLayer(StoryNode("Hello Driver", {
    "q": { 'text': "start over", 'connection': [0,0] },
}), 6)
story.addToLayer(StoryNode("Hello Driver", {
    "q": { 'text': "start over", 'connection': [0,0] },
}), 6)
story.addToLayer(StoryNode("Hello Driver", {
    "q": { 'text': "start over", 'connection': [0,0] },
}), 6)
story.addToLayer(StoryNode("Hello Driver", {
    "q": { 'text': "start over", 'connection': [0,0] },
}), 6)
story.addToLayer(StoryNode("Hello Driver", {
    "q": { 'text': "start over", 'connection': [0,0] },
}), 6)
story.addToLayer(StoryNode("Hello Driver", {
    "q": { 'text': "start over", 'connection': [0,0] },
}), 6)
story.addToLayer(StoryNode("Hello Driver", {
    "q": { 'text': "start over", 'connection': [0,0] },
}), 6)
story.addToLayer(StoryNode("Hello Driver", {
    "q": { 'text': "start over", 'connection': [0,0] },
}), 6)
story.addToLayer(StoryNode("Hello Driver", {
    "q": { 'text': "start over", 'connection': [0,0] },
}), 6)

story.start()