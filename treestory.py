#TODO - CREATE QUIT COMMAND

class TreeStory():
    def __init__(self, layers):
        self.structure = []
        if layers:
            while len(self.structure) < layers:
                self.newLayer()

    def newLayer(self):
        self.structure.append([])

    def addToLayer(self, node, layer, index=""):
        if index:
            self.structure[layer].insert(index, node)
        else:
            self.structure[layer].append(node)

    def getNode(self, layer, index):
        return self.structure[layer][index]

    def playNode(self, layer, index):
        node = self.getNode(layer, index)
        if node:
            print(" ")
            print(node.text)
            print(" ")
            self.promptUser(node.options)

            
        else:
            print("Failed to get node " + layer + "_" + index)

    def promptUser(self, options):
        print("*--Options--*")
        for key, option in options.items():
            print("     " +key + ": " + option["text"])        

        result = input("SELECT: ")

        if result:
            if keyIsInDictionary(result, options):
                connection = options[result]["connection"]
                self.playNode(connection[0], connection[1])
            else:
                self.illegalInputPrompt(options)
        else:
            self.illegalInputPrompt(options)

    def illegalInputPrompt(self, options):
        print("ILLEGAL COMMAND. TRY AGAIN.")
        self.promptUser(options)

    def start(self):
        self.playNode(0,0)

class StoryNode():
    def __init__(self, text, options):
        self.text = text
        if type(options).__name__ == "dict":
            self.options = options


def keyIsInDictionary(key, dict):

    for dict_key, value in dict.items():
        if key == dict_key:
            return True

    return False
